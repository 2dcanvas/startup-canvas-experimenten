Installation
-
This library requires jQuery ro run, to install from cdn, paste the following into your html:

`<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>`

Make sure to include jQuery before including the jscroll library

Usage
-
After including the library make sure you have a div in your html with id `container`, div inside this container must set their position relative to their parent container div.

Click on the page to create text, this text can be moved by holding the ctrl key and left mousebutton and dragging yourm ouse.
The page can be scrolled by holding the left mouse button and dragging.
Zooming can be done via the mousewheel, as well as with your page down & page up keys.
