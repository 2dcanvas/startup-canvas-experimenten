'use strict';

//var inputdivCount = 0;
	
				//inputdivCount += 1;
				
		function createTextInput (inputdivCount, baseX, baseY) {
				
				$("<div>", {
					"id" : "inputdiv" + inputdivCount,  
					
					
					css : {
						"top" : baseY + "px", "left" : baseX - 80 + "px", "position" : "absolute"
					}
					
					
				}).appendTo("#canvas");
				
				$("<textarea>", {
					
					"id" : inputdivCount, 
					
				}).appendTo("#inputdiv"+inputdivCount+"");
				
				$("<a>", {
					
					"href" : "javascript:void(0)", "text" : "Undo ", "id" : "undo"+inputdivCount, 
					
				}).appendTo("#inputdiv"+inputdivCount+"");
				
				$("<a>", {
					
					"href" : "javascript:void(0)", "text" : "Redo", "id" : "redo"+inputdivCount, 
					
				}).appendTo("#inputdiv"+inputdivCount+"");
				
	 $("textarea").click(function(e) {
		 
		 				var setEditorContents = function(contents) {

    $('#'+e.target.id).val(contents);
};
			
			
	var history = new SimpleUndo({
		maxLength: 200,
		provider: function(done) {
			done($('#'+e.target.id).val());
		},
		onUpdate: function() {
			//onUpdate is called in constructor, making history undefined
			if (!history) return; 
		
		}
	});

	$('#undo'+e.target.id).click(function() {
		history.undo(setEditorContents);
	});
	$('#redo'+e.target.id).click(function() {
		history.redo(setEditorContents);
	});
	$('#'+e.target.id).keypress(function() {
			console.log(history);
		history.save();
	});	
	
	
	 });
}