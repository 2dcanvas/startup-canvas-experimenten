/**
 * Created by K. Nobel on 2/9/2016.
 */

function openImageBrowser() {
    jQuery('#imageBrowse').trigger('click');
}

$(document).ready(function (e) {
    $('#myForm').on('submit',(function(e) {
        e.preventDefault();

        $.ajax({
            url: 'saveImage.php',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                var image = $('#result');
                if(data.substring(0, 7) == "ERROR: ") {
                    image.css("display", "none");
                    console.log(data);
                } else {
                    var placedImage = $("<img>",{
                        "class":"image",
                        "src":data
                    }).appendTo("#canvas");

                    var img = new Image();
                    img.src = data;
                    $(img).on("load", function() {
                        var dimensions = calculateAspectRatioFit(img.width, img.height, 250, 250);
                        placedImage.css("width", dimensions[0]);
                        placedImage.css("height", dimensions[1]);
                        placedImage.css("left", lastMousePosition[0] - dimensions[0] / 2);
                        placedImage.css("top", lastMousePosition[1] - dimensions[1] / 2);
                    });
                }
            }
        })
    }));

    $('#imageBrowse').on('change', function() {
        $('#myForm').submit();
    });
});

function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    console.log("srcwidth: " + srcWidth + "\n" +
        "srcheight: " + srcHeight + "\n" +
        "maxwidth: " + maxWidth + "\n" +
        "maxheight: " + maxHeight + "\n");


    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    console.log(ratio);


    var width = srcWidth * ratio;
    var height = srcHeight * ratio;
    console.log(width);
    return [width, height];
}