# Team Invoer #

Hieronder staat de documentatie van ons deel van Roomz.

### Bestanden index ###

1. css/canvas.css
2. images
3. js/createTextInput.js
4. js/menu.js
5. js/saveImage.js

6. lib/jquery.js
7. lib/undoRedoStack.js

8. saveImage.php
9. index.html


### 1. canvas.css ###

* De canvas CSS is gesplitst in twee gedeeltes. Namelijk het menu linksboven en de rest van het canvas (menuwaaier etc.).

* De CSS sheet is voorzien van twee comment titels om de twee verschillende gedeeltes aan te duiden.

### 2. images ###

* In deze map staan alle gebruikte afbeeldingen. Momenteel zijn dat alleen de icoontjes van de menuwaaier en het menu linksboven.

### 3. createTextInput.js ###

* Bevat een functie die in js/menu.js wordt ingeladen. 
* De functie maakt een textarea aan met een eigen Undo - Redo stack waardoor alle textareas apart van elkaar een invoergeschiedenis hebben

* * NOTE: de Undo - Redo stack is afhankelijk van een library die staat in lib/undoRedoStack.js. Deze library doet niks anders dan het bijhouden van de wijzigingen van de textareas.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact