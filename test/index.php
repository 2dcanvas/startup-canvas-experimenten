<!DOCTYPE html>
<html> 
  <head> 
    <script src="jquery.js"></script> 
    <script src="konva.min.js"></script>
    <script> 
    $(function(){
      $("#includedContent").load("includes/canvas.php"); 
    });
    </script> 
  </head> 

  <body> 
     <div id="includedContent"></div>
  </body> 
</html>