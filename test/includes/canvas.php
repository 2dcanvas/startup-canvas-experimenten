<html>
	<head>
		<style type="text/css" media="screen">
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				text-align: center;
			}
		</style>
	</head>
	<body>
		Tool:
		<select id="tool">
			<option value="brush">Brush</option>
			<option value="eraser">Eraser</option>
		</select>
		<div id="container"></div>
		<script>
			var width = window.innerWidth;
			var height = window.innerHeight - 25;

			// first we need Konva core things: stage and layer
			var stage = new Konva.Stage({
				container : 'container',
				width : width,
				height : height
			});

			var drawlayer = new Konva.Layer();
			var boxlayer = new Konva.Layer();

			stage.add(drawlayer);

			// then we are going to draw into special canvas element
			var canvas = document.createElement('canvas');
			canvas.width = stage.width();
			canvas.height = stage.height();

			// created canvas we can add to layer as "Konva.Image" element
			var image = new Konva.Image({
				image : canvas,
				x : 0,
				y : 0,
				//stroke : 'black'
			});
			drawlayer.add(image);
			stage.draw();

			// Good. Now we need to get access to context element
			var context = canvas.getContext('2d');
			context.strokeStyle = "#000000";
			context.lineJoin = "round";
			context.lineWidth = 5;

			var isPaint = false;
			var lastPointerPosition;
			var mode = 'brush';

			// now we need to bind some events
			// we need to start drawing on mousedown
			// and stop drawing on mouseup
			stage.on('contentMousedown.proto', function() {
				isPaint = true;
				lastPointerPosition = stage.getPointerPosition();

			});

			stage.on('contentMouseup.proto', function() {
				isPaint = false;
			});

			// and core function - drawing
			stage.on('contentMousemove.proto', function() {

				if (!isPaint) {
					return;
				}
				if (onBox) {
					return;
				}

				if (mode === 'brush') {
					context.globalCompositeOperation = 'source-over';
					context.beginPath();

					var localPos = {
						x : lastPointerPosition.x - image.x(),
						y : lastPointerPosition.y - image.y()
					};
					context.moveTo(localPos.x, localPos.y);
					var pos = stage.getPointerPosition();
					localPos = {
						x : pos.x - image.x(),
						y : pos.y - image.y()
					};
					context.lineTo(localPos.x, localPos.y);
					context.closePath();
					context.stroke();

					lastPointerPosition = pos;
					drawlayer.draw();
				}
				if (mode === 'eraser') {
					context.globalCompositeOperation = 'destination-out';
					context.beginPath();

					var localPos = {
						x : lastPointerPosition.x - image.x(),
						y : lastPointerPosition.y - image.y()
					};
					context.moveTo(localPos.x, localPos.y);
					var pos = stage.getPointerPosition();
					localPos = {
						x : pos.x - image.x(),
						y : pos.y - image.y()
					};
					context.lineTo(localPos.x, localPos.y);
					context.closePath();
					context.stroke();

					lastPointerPosition = pos;
					drawlayer.draw();
				}
			});

			var select = document.getElementById('tool');
			select.addEventListener('change', function() {
				mode = select.value;
			});

			//rectangle
			var rectX = stage.getWidth() / 2 - 50;
			var rectY = stage.getHeight() / 2 - 25;

			var box = new Konva.Rect({
				x : rectX,
				y : rectY,
				width : 100,
				height : 50,
				fill : '#00D2FF',
				stroke : 'black',
				strokeWidth : 4,
				draggable : true
			});
			var onBox = false;

			// add cursor styling
			box.on('mouseover', function() {
				document.body.style.cursor = 'pointer';				

			});
			box.on('mouseout', function() {
				document.body.style.cursor = 'default';
			});
			box.on('mousedown', function() {
				mode = '';
				onBox = true;
			});
			box.on('mouseup', function() {
				mode = select.value;
				onBox = false;
				
			});

			boxlayer.add(box);
			stage.add(boxlayer);

		</script>
	</body>
</html>